# [HacDC](https://hacdc.org/) / [PAR Recycle Works](https://home.par-recycleworks.org/) Partnership

Herein are ideas regarding introductory computer classes, to be
offered by **HacDC**, for returning citizens transitioning via
**People Advancing Reintegration (PAR) Recycle Works**.

The conversation between HacDC and PAR began [insert date] followed by
a visit to Philadelphia and meetings between PAR, HacDC, DC government
folks, PSA-703, and Vehicles for Change / Full Cicle training which
took place on 2021.07.23.

----

## 2021.07.29

* The [Getting Down
  With](http://www.openbookproject.net/tutorials/getdown/) collection
  provides good introductions to:

    + the [Linux Command Line Interface (CLI)](http://www.openbookproject.net/tutorials/getdown/unix/)
	+ [HTML](http://www.openbookproject.net/tutorials/getdown/html/)
	+ [CSS](http://www.openbookproject.net/tutorials/getdown/css/)
	+ and more

* I strongly recommend skipping the **vim** lesson in the **Linux
  CLI** course, but **nano** should be covered -- even if it is not
  the editor that students will use most.

* For Python, with variing degrees of obsolescence:

    + [Python Bibliotheca](http://www.openbookproject.net/pybiblio/)
    + [Python for Fun](http://www.openbookproject.net/py4fun/)
    + [Graphics API for Students of Python (GASP)](http://www.openbookproject.net/pybiblio/gasp/course/)

----
